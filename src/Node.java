
import java.util.*;

import static com.sun.org.apache.xml.internal.utils.XMLCharacterRecognizer.isWhiteSpace;
import static java.lang.Character.isLetterOrDigit;

/*
 * Code inspiration from:
 * https://www.geeksforgeeks.org/expression-tree/
 * https://enos.itcollege.ee/~jpoial/algorithms/examples/TreeNode.java
 * https://github.com/egaia/AlgorithmAndDataStructure/blob/master/home5/src/Node.java
 *
 */

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node d, Node r) {
      this.name = n;
      this.firstChild = d;
      this.nextSibling = r;
   }
   
   public static Node parsePostfix (String s) {
      for (int a = 0; a < s.length() -1; a++){
         if (isLetterOrDigit(s.charAt(a)) && isWhiteSpace(s.charAt(a + 1))){
            throw new RuntimeException("Whitespace in string: " + s);
         }
      }
      if (s.contains(",,")) {
         throw new RuntimeException("Double commas in string: " + s);
      }
      if (s.contains("\t")) {
         throw new RuntimeException("Tab in string: " + s);
      }

      if (s.contains("()")) {
         throw new RuntimeException("Empty subtree in string: " + s);
      }
      if (s.contains("( , ) ")) {
         throw new RuntimeException("Brackets and comma in string: " + s);
      }
      if(s.isEmpty()){
         throw new RuntimeException("Empty string: " + s);
      }
      if (s.contains("((") && s.contains("))")) {
         throw new RuntimeException("Double brackets in string: " + s);
      }
      if (s.contains(",") && !s.contains("(") && !s.contains(")")) {
         throw new RuntimeException("Two roots in string: " + s);
      }
      if (s.contains("(,") || s.contains("),")) {
         throw new RuntimeException("Bracket followed by comma in string : " + s);
      }
      if(s.matches("[)]\\w+[(]"))
         throw new RuntimeException("Wrong brackets in string: " + s );

      Stack<Node> stack = new Stack<>();
      Node newNode = new Node(null, null, null);
      StringTokenizer st = new StringTokenizer(s, "(),", true);

      while(st.hasMoreTokens()){
         String token = st.nextToken().trim();
         switch (token) {
            case "(" : {
               stack.push(newNode);
               newNode.firstChild = new Node(null, null, null);
               newNode = newNode.firstChild;
                continue;
            }
            case ")" : {
               newNode = stack.pop();
               continue;
            }
            case "," : {
               if (stack.empty())
                  throw new RuntimeException("Error with comma in string: " + s);
               newNode.nextSibling = new Node(null, null, null);
               newNode = newNode.nextSibling;
               continue;
            }
            default : newNode.name = token;
         }
      }
      return newNode;
   }

   public String leftParentheticRepresentation() {
      StringBuffer b = new StringBuffer();
      b.append(this.name);
      if (this.firstChild != null) {
         b.append("(");
         b.append(this.firstChild.leftParentheticRepresentation());
         b.append(")");
      }

      if (this.nextSibling != null) {
         b.append(",");
         b.append(this.nextSibling.leftParentheticRepresentation());
      }
      return b.toString();
   }

   public static void main (String[] param) {
//      String s = "%%";            // ok
//       String s = "((@,#)+)-34";   // ok
//       String s = "((@, #)+)-34";  // ok
//       String s = "     \t ";      // not ok
//       String s = "((1),(2)3)4";   // not ok
//       String s = "A B";           // not ok
       String s = "((3 4, 5)6)7";  // not ok
      System.out.println ("Input: " + s);
      Node tree = Node.parsePostfix (s);
      System.out.println (" Left: "
              + tree.leftParentheticRepresentation());
   }

//   public static void main (String[] param) {
//      String s = "(B1,C)A";
//      Node t = Node.parsePostfix (s);
//      String v = t.leftParentheticRepresentation();
//      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
//   }
}

